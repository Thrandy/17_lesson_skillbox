#include <iostream>

class Vector 
{
public:
    Vector() : x(0), y(0), z(0)
    {}
    Vector(double _x, double _y, double _z) : x(_x), y(_y), z(_z)
    {}

    void Set(double _xx, double _yy, double _zz)
    {
        x = _xx;
        y = _yy;
        z = _zz;
    }

    void Show()
    {
        std::cout << "(" << x << ", " << y << ", " << z << ")";
    }

    double Length()
    {
        return sqrt(x * x + y * y + z * z);
    }



private:
    double x;
    double y;
    double z;
};

int main()
{
    Vector ForceVector(3, 4, 5);

    std::cout << "The vector length of ";
    ForceVector.Show();
    std::cout << " = " << ForceVector.Length() << "\n";


}